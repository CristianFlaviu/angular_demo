import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  public formGroup: FormGroup;
  public users: any[] = [];
  public name: any;
  constructor(private testService: TestService, private formBuilder: FormBuilder,) { }

  ngOnInit(): void {

    this.testService.getAllUsers().subscribe(

      (data: any) => {
        console.log(data);
        this.users = data
      },
      (err) => { console.log(err) }
    );
  }
}
